var pos_s = document.getElementsByClassName('pos_s')[0]

$('._menuTrig').on('click', function (e) {
    e.stopPropagation()
    if ($('._menuTrig').next('.dropM').hasClass('show')) {
        $('.dropM').removeClass('show')
        $('._menuTrig').attr("aria-expanded", "false");
    } else {
        $('.dropM').addClass('show')
        $('._menuTrig').attr("aria-expanded", "true");

        var activeItem = $('.level2.active').parents('ul.main_categories').outerHeight(),
            activeItemChild = $('.level2.active').find('.main_categories_wrap_right').outerHeight();

        if (activeItem < activeItemChild) {
            $('.level2.active').parents('ul.main_categories').css({
                "min-height": activeItemChild - 70
            });
        } else {
            $('.level2.active').parents('ul.main_categories').css({
                "min-height": activeItem
            });
        }

    }
})

$(document).on('click', function (e) {
    if ($('.dropM').hasClass('show') && !$(e.target).parents('.dropM').length && !$(e.target).parents('._menuTrig').length) {
        $('.dropM').removeClass('show')
        $('._menuTrig').attr("aria-expanded", "false");
    }
})

var disableScroll = false;
var scrollPos = 0;

function stopScroll() {
    disableScroll = true;
    scrollPos = $(window).scrollTop();
}

function enableScroll() {
    disableScroll = false;
}

jQuery(function ($) {

    if ($('._section_h100').length) {
        var scrll_controller1 = new ScrollMagic.Controller();
    }

    // var sec1 = new ScrollMagic.Scene({
    //     triggerElement: '#section_1',
    //     duration: '100%',
    // })
    // .addIndicators({
    //     name: 'Section 1'
    // })
    // .on('start', function(event){
    // })
    // .on('leave', function(event) {
    //     if (event.scrollDirection == 'FORWARD') {
    //         $('#section_1').css({
    //             'z-index': 0
    //         })
    //     } else {
    //         $('#section_1').removeAttr('style')
    //     }
    // })
    // .addTo(scrll_controller1);



    var trig = true;



    function scrollIt() {
        $('._section_h100').each(function (index, el) {

            $(el).outerHeight($(el).find('.insider').outerHeight() * 2)

            var durationX2 = $(el).find('.insider').outerHeight() * 2;

            if ($(el).find('.insider').outerHeight() <= $(window).height()) {
                scene = new ScrollMagic.Scene({
                    triggerHook: .5,
                    triggerElement: el,
                    duration: durationX2
                })
                    .on('enter', function (event) {
                        // console.log(trig)
                        if (event.scrollDirection == 'FORWARD' && trig) {
                            $(el).siblings('._section_h100').removeClass('activeScene-toTop activeScene-toBottom prevScene nextScene currentScene')
                            $(el).addClass('currentScene')
                            $(el).removeClass('prevScene nextScene activeScene-toTop activeScene-toBottom')


                            $('.activeScene-toBottom_noAnimation').removeClass('activeScene-toBottom_noAnimation');

                            $(el).addClass('activeScene-toTop');
                            $(el).next('._section_h100').addClass('nextScene');
                            $(el).prev('._section_h100').addClass('prevScene');

                            if ($(el).index() == $('._section_h100').length) {
                                $(el).prev('._section_h100').addClass('nextScene');
                            }
                        } else if (event.scrollDirection == 'REVERSE') {
                            $(el).siblings('._section_h100').removeClass('activeScene-toTop activeScene-toBottom prevScene nextScene currentScene')
                            $(el).addClass('currentScene')
                            $(el).removeClass('prevScene nextScene activeScene-toTop activeScene-toBottom')


                            $(el).addClass('activeScene-toBottom currentScene');
                            $(el).prev('._section_h100').addClass('nextScene');
                            $(el).next('._section_h100').addClass('prevScene');

                            if ($(el).index() == 0) {
                                $(el).next('._section_h100').addClass('nextScene');
                            }
                        }
                    })
                    .on('leave', function (event) {
                        trig = true;
                        $(el).siblings('._section_h100').removeClass('activeScene-toTop activeScene-toBottom prevScene nextScene')
                        if (event.scrollDirection == 'FORWARD' && $(el).index() == 0) {
                            $('.activeScene-toBottom_noAnimation').removeClass('activeScene-toBottom_noAnimation')

                            $(el).removeClass('activeScene-toTop prevScene');
                            $(el).next('._section_h100').addClass('prevScene');
                        }
                    })
                    .on('progress', function (event) {

                        $('.progressBar').innerWidth(event.progress * 100 + '%')

                    })
                    .addTo(scrll_controller1)
            }
        });

        $('.__section_h100').each(function (index, el) {

            $(el).find('.insider').css({
                'padding-top': $(window).height() / 3,
                'padding-bottom': $(window).height() / 2,
            })

            $(el).outerHeight($(el).find('.insider').outerHeight());

            if ($(el).find('.insider').outerHeight() > $(window).height()) {
                _scene = new ScrollMagic.Scene({
                    triggerHook: 1,
                    triggerElement: el,
                    duration: $(el).find('.insider').outerHeight()
                })
                    .on('progress', function (event) {
                        $('.progressBar').innerWidth(event.progress * 100 + '%')

                        if (event.scrollDirection == 'FORWARD' && _scene.progress() >= 0.5 && trig) {
                            trig = false;
                            $(el).siblings('._section_h100').removeClass('activeScene-toTop activeScene-toBottom prevScene nextScene')
                            $(el).removeClass('prevScene nextScene')
                            $(el).next().addClass('activeScene-toTop');
                        } else if (event.scrollDirection == 'REVERSE' && _scene.progress() < 0.7) {
                            trig = true;
                            $(el).siblings('._section_h100').removeClass('activeScene-toTop activeScene-toBottom prevScene')
                            $(el).removeClass('prevScene')
                            $(el).prev().addClass('activeScene-toBottom_noAnimation');
                        }
                    })
                    .addTo(scrll_controller1)
            }
        });
    }


    if ($(window).outerHeight() >= 969) {
        scrollIt()
        window.addEventListener('resize', scrollIt())
    }


    $('._scroll_to_A').on('click', function (event) {
        event.preventDefault();
        var scrll_to = parseInt($($(event.target).parent('a').attr('href')).offset().top);

        $('html, body').stop(true, true).animate({ scrollTop: scrll_to }, 400);
    })

    $('._scroll_to').on('click', function (event) {
        event.preventDefault();
        var scrll_to = parseInt($($(event.target).parent('a').attr('href')).offset().top);

        // scrll_controller.scrollTo(scrll_to);

        // ++ for scrollspy
        scrll_to += 20;
        // if ($($(event.target).parent('a').attr('href')).outerHeight() <= $(window).outerHeight()) {
        // }
        // -- for scrollspy

        $('html, body').stop(true, true).animate({ scrollTop: scrll_to }, 0);
    })

    // ++ scroll to top

    // var scroll_top;
    function toggle_scroll_button() {
        var scroll_top = parseInt($(window).scrollTop());
        if (scroll_top > 200) {
            $('.scroll-to-top').show();
            $('.mainNavigation').addClass('scrolled')
        } else {
            $('.scroll-to-top').hide();
            $('.mainNavigation').removeClass('scrolled')
        }
    }
    toggle_scroll_button();
    $(window).on('scroll', function () {
        toggle_scroll_button();
    })
    $('._scroll-to-top, .scroll-to-top').on('click', function (event) {
        event.preventDefault();
        var scroll_top = parseInt($(window).scrollTop());
        if (scroll_top > 600) {
            $('body, html').animate({ scrollTop: 500 }, 1);
        }
        $('body, html').animate({ scrollTop: 0 }, '200', function () {
            $('.scroll-to-top').hide();
        });
    });

    // -- scroll to top

    $('.popupNavTrigger').on('click', function () {
        $(this).toggleClass('is-active');

        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });

    $(window).bind('scroll', function () {
        if (disableScroll) $(window).scrollTop(scrollPos);
    });
    $(window).bind('touchmove', function () {
        $(window).trigger('scroll');
    });
    var ps1,
        ps2;
    $('._level_1 span').on('click', function (event) {
        event.stopPropagation();
        ps1 = new PerfectScrollbar('._scrollable', {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('.hasChild').on('click', function (event) {
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $(this).find('.childItem').first().addClass('active');
    });
    $('.childItem__goBackBtn span').on('click', function (event) {
        event.stopPropagation();
        $(this).parents('.childItem').first().removeClass('active');
    });
    $('.mPopupNavScroll').each(function (index, el) {
        const ps = new PerfectScrollbar(this, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    });
    $('._close').on('click', function () { });

    $('.mPopupNav').on('hide.bs.modal', function () {
        if (ps1) {
            ps1.destroy();
        }
        if (ps2) {
            ps2.destroy();
        }
        $('.popupNavTrigger').removeClass('is-active');
        $('._popupNav').find('.childItem').each(function () {
            $(this).removeClass('active');
        });
    })
    $('._pass_visibility_trig').on('click', function () {
        var pass = $(this).parents('.pass_field').find('#pass');
        if (pass.attr('type') == 'password') {
            pass.attr('type', 'text');
        } else {
            pass.attr('type', 'password');
        }
        $(this).find('.pas_icon_trig').toggle();
    })

    function scrollItAll(whatToScroll, whatToDo) {
        var ps1_scrollable_content;
        ps1_scrollable_content = new PerfectScrollbar(whatToScroll, {
            wheelSpeed: 1,
            wheelPropagation: false,
            suppressScrollX: true
        });
    }

    if ($('._scrollable_content').length) {
        $('._scrollable_content').each(function (index, el) {
            var k = '._scrollable_content' + index;
            scrollItAll(k)
        });
    }


    //set defaults for modals
    var scrollWidth = window.innerWidth - document.documentElement.clientWidth;
    var modalIsOpen = false,
        modalIsAnimated = false;

    function simpleModalShow(target) {
        if ($(target).length && !modalIsAnimated) { //no act while animating is in proggress or no target specified
            modalIsOpen = true;
            $('body').addClass('modal-open');
            $('body').css('padding-right', scrollWidth);
            $(target).velocity("fadeIn", {
                duration: 300,
                begin: function () {
                    modalIsAnimated = true;
                    $(target).trigger('modal_show');
                },
                complete: function () {
                    modalIsAnimated = false;
                    $(target).trigger('modal_shown');
                }
            });
            $(target).removeClass('fade');
            $(target).addClass('show');
        } else {
            console.error('the target modal does not exist');
        }
    }

    function simpleModalHide() {

        if (!modalIsAnimated) { //no act while animating is in proggress
            $('.modal.show').velocity("fadeOut", {
                duration: 300
            }, {
                begin: function () {
                    modalIsAnimated = true;
                    $('.modal.show').trigger('modal_hide');
                },
                complete: function () {
                    $('body').removeClass('modal-open');
                    $('body').css('padding-right', '');
                    $('.modal.show').removeClass('show');
                    modalIsOpen = false;
                    modalIsAnimated = false;
                    $('.modal.show').trigger('modal_hidden');
                }
            });
        }
    }

    $(document).on('click', '.toggleModal', function (event) {
        event.preventDefault();
        var target = $(this).data('target');

        if (modalIsOpen) {
            simpleModalHide();
        } else {
            simpleModalShow(target);
            $('body').addClass('scrolledUp');
        }
    });

    $('.modal:not(._popupNav), ._closeBtn').on('click', function (event) {
        if ($(event.target).attr('class') == $(this).attr('class')) { //work around bubbling
            event.preventDefault();
            simpleModalHide();
        }
    });

});







var scrollWidth = window.innerWidth - document.documentElement.clientWidth;
var ps;

function navPopupHide(toHide, reopen, caller, target) {
    var popupWidth = $('body').outerWidth() / 100 * 75;
    var popupHeight = $('.popupNav_horizontal').outerHeight() / 100 * 75;
    if ($('body').hasClass('horizontalNav')) {
        if (reopen) {
            //toggle mainNav when the one is already open
            $(toHide).css('transform', 'translateY(-' + popupHeight + 'px)');
            $(toHide).animate({ 'opacity': '0' }, 250);
            $(toHide).removeClass('showNav');
            $('._closeCaller').find('._nav_icon').removeClass('_close');
            $('._closeCaller').removeClass('_closeCaller active');
            navPopupShow(caller, target);
        } else {
            if (!$('body').hasClass('navAnimated')) {
                $('.showNav').css('transform', 'translateY(-' + popupHeight + 'px)');
                $('._closeCaller').find('._nav_icon').removeClass('_close');
                $('._closeCaller').removeClass('_closeCaller active');
                $('.moover').css('transform', 'translateX(0)');
                $('.popupNav_bg').css('opacity', '0');

                setTimeout(function () {
                    $('body').css('padding-right', 0);
                    $('body').removeClass('navOpen');
                    $('body').removeClass('navAnimated');
                    $('.showNav').removeClass('showNav');
                }, 400);

                $('body').addClass('navAnimated');
            }
        }
    } else {
        if (reopen) {
            //toggle mainNav when the one is already open
            $(toHide).css('transform', 'translateX(-' + popupWidth + 'px)');
            $(toHide).animate({ 'opacity': '0' }, 250);
            $(toHide).removeClass('showNav');
            $('._closeCaller').find('._nav_icon').removeClass('_close');
            $('._closeCaller').removeClass('_closeCaller active');
            navPopupShow(caller, target);
        } else {
            if (!$('body').hasClass('navAnimated')) {
                $('.showNav').css('transform', 'translateX(-' + popupWidth + 'px)');
                $('._closeCaller').find('._nav_icon').removeClass('_close');
                $('._closeCaller').removeClass('_closeCaller active');
                $('.moover').css('transform', 'translateX(0)');
                $('.popupNav_bg').css('opacity', '0');

                setTimeout(function () {
                    $('body').css('padding-right', 0);
                    $('body').removeClass('navOpen');
                    $('.insider:not(.static_insider)').css({ 'width': 'calc(100% - 170px)' });
                    $('body').removeClass('navAnimated');
                    $('.showNav').removeClass('showNav');
                }, 400);

                $('body').addClass('navAnimated');
            }
        }
    }
    // removing perfectScroll to prevent doubling
    if (ps) {
        ps.destroy();
    }
}

function navPopupShow(caller, target) {
    $(target).css('opacity', '1');
    if ($('body').hasClass('horizontalNav')) {
        if (!$('body').hasClass('navAnimated')) {
            var navHeight = $('.mainNavigation').outerHeight();
            caller.addClass('_closeCaller active');
            caller.find('._nav_icon').addClass('_close');
            target.css('transform', 'translateY(' + navHeight + 'px)');
            target.addClass('showNav');
            $('.popupNav_bg').css('opacity', '1');
            $('body').addClass('navOpen');
            $('body').css('padding-right', scrollWidth);
            $('body').addClass('navAnimated');
            setTimeout(function () {
                $('body').removeClass('navAnimated');
                scroller();
            }, 200);
        }
    } else {
        if (!$('body').hasClass('navAnimated')) {
            var navWidth = $('.mainNavigation').outerWidth();
            caller.addClass('_closeCaller active');
            caller.find('._nav_icon').addClass('_close');
            $('.moover').css('transform', 'translateX(400px)');
            target.css('transform', 'translateX(' + navWidth + 'px)');
            target.addClass('showNav');
            $('.popupNav_bg').css('opacity', '1');
            $('body').addClass('navOpen');
            $('body').css('padding-right', scrollWidth);
            $('.insider:not(.static_insider)').css({ 'width': 'calc(100% - ' + (170 + scrollWidth) + 'px)' });
            $('body').addClass('navAnimated');

            setTimeout(function () {
                $('body').removeClass('navAnimated');
                scroller();
            }, 200);
        }
    }
}

$('#popupNav_bg').on('click', function (event) {
    event.preventDefault();
    navPopupHide();
});

function navResize(navWidth, navPopupWidth) {
    $('._popupNav').addClass('no_transition'); // to prevent unexpected transitions after window resize
    if (navState()) {
        $('.showNav').css('transform', 'translateX(' + navWidth + 'px)');
        if (ps) {
            ps.destroy();
        }
        scroller();
        $('.showNav').siblings('._popupNav').css('transform', 'translateX(-' + navPopupWidth + 'px)');
    } else {
        $('._popupNav:not(.mPopupNav)').css('transform', 'translateX(-' + navPopupWidth + 'px)');
        if (ps) {
            ps.destroy();
        }
    }
    setTimeout(function () {
        $('._popupNav').removeClass('no_transition');
    }, 10);
}

$(window).resize(function (event) {
    // recalc popup position on window resize
    var navWidth = $('.mainNavigation').outerWidth(),
        navPopupWidth = $('._popupNav').outerWidth();
    navResize(navWidth, navPopupWidth);
});

function scroller() {
    if ($(window).outerHeight() < $('.showNav .popupNav__inner').outerHeight()) {
        //turn perfectScroll on (main navigation popup)
        var toScroll = '#' + $('.showNav').attr('id');
        ps = new PerfectScrollbar(toScroll, {
            wheelSpeed: 2,
            wheelPropagation: true,
            minScrollbarLength: 20
        });
    }
}

$('._popup_call_btn').on('click', function (event) {
    event.preventDefault();
    var caller = $(this),
        target = $(caller.data('target'));
    if (!$('body').hasClass('navAnimated')) {
        if (!navState()) {
            navPopupShow(caller, target);
        } else if (!caller.hasClass('active')) {
            var toHide = caller.parents('.mainNavigation').find('.active').data('target');
            navPopupHide(toHide, true, caller, target);
            navPopupShow(caller, target);
        } else {
            navPopupHide();
        }
    }
});

// check if main navigation popup is shown
function navState() {
    var result = $('body').hasClass('navOpen');
    return result;
}



// ++ mainPage slider

function swipedetect(el, callback) {

    var touchsurface = el,
        swipedir = 'none',
        startX,
        startY,
        distX,
        distY,
        threshold = 50, //required min distance traveled to be considered swipe
        restraint = 50, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 300, // maximum time allowed to travel that distance
        elapsedTime,
        startTime,
        handleswipe = callback;

    touchsurface.addEventListener('touchstart', function (e) {
        var touchobj = e.changedTouches[0];
        swipedir = 'none';
        var dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime();
        // record time when finger first makes contact with surface
    }, false)

    touchsurface.addEventListener('touchend', function (e) {
        var touchobj = e.changedTouches[0]
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = parseInt(new Date().getTime()) - parseInt(startTime) // get time elapsed
        if (elapsedTime <= allowedTime) { // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
                swipedir = (distX < 0) ? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
                // e.preventDefault()
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
                swipedir = (distY < 0) ? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
            }
        }

        handleswipe(swipedir)
    }, false)

    touchsurface.addEventListener('mousedown', function (e) {
        swipedir = 'none';
        startX = e.pageX;
        startY = e.pageY;
        // e.target.parentNode.style.opacity = '.5'
    })

    touchsurface.addEventListener('mouseup', function (e) {

        distX = e.pageX - startX;
        distY = e.pageY - startY;

        if (Math.abs(distX) >= threshold && Math.abs(distY) <= 100) { // 2nd condition for horizontal swipe met
            swipedir = (distX < 0) ? 'left' : 'right' // if dist traveled is negative, it indicates left swipe
        }
        else if (Math.abs(distY) >= threshold && Math.abs(distX) <= 100) { // 2nd condition for vertical swipe met
            swipedir = (distY < 0) ? 'up' : 'down' // if dist traveled is negative, it indicates up swipe
        }
        handleswipe(swipedir)
    })
}

$('.uvSlider').each(function (index, el) {
    for (var i = 0; i < $(el).find('.uvSliderItem').length; i++) {
        $(el).find('.uvSlider__pagination').append('<li class="uvSlider__pagination__item navSonar"><button class="sr-only" aria-controls="carousel">Toggle slide ' + (i + 1) + '</button></li>');
    }
    $(el).find('.uvSlider__pagination__item').eq($('.uvSliderItem.active').index()).addClass('active');
});

$('.uvSliderItem.active').each(function (index, el) {
    $(el).velocity({
        opacity: 1,
        complete: function () {
            $(el).attr('aria-hidden', 'false')
        }
    })
});

function uvSlideHide(act, dir) {
    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    act.find('.uvSliderItem__mainText').velocity({
        translateX: -294 * d,
    }, {
        duration: 800
    });
    act.find('.uvSliderItem__mainImage').velocity({
        translateX: -294 * d,
    }, {
        duration: 700
    });
    act.velocity({
        opacity: [0, 1],
        complete: function () {
            act.attr('aria-hidden', 'true')
        }
    }, {
        duration: 800
    });
}

function uvSlideShowNext(act, nextAct, slideImage, slideText, dir) {

    var d = 1;
    if (dir == 'prev') {
        d = -1;
    }

    nextAct.addClass('active');

    slideText.velocity({
        opacity: 0
    }, {
        duration: 0
    });

    nextAct.velocity({
        // translateX: [0, 294 * d],
        opacity: [1, 0],
        complete: function () {
            nextAct.attr('aria-hidden', 'false')

            if ($('._height_recalc').length) {
                nextAct.parents('.uvSliderItemsList').outerHeight(nextAct.find('._height_recalc').outerHeight())
            }
        }
    }, {
        duration: 800
    });

    nextAct.find('.ghost_text').velocity({
        translateX: [0, 294 * d],
        opacity: [1, 0],
    }, {
        duration: 1200,
        easing: [0.24, 0.57, 0.47, 0.8]
    });

    nextAct.find('.uvSliderItem__background').velocity({
        scale: [1, 1.1]
    }, {
        duration: 2000
    });

    slideImage.velocity({
        translateX: [0, -200 * d],
        opacity: [1, 0]
    }, {
        duration: 800
    });

    slideText.velocity({
        translateX: [0, -124 * d],
        opacity: [1, 0]
    }, {
        duration: 800
    });
}

function initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent) {

    if (uvItemsLength > 1) {


        activeItem.removeClass('active');

        $('.velocity-animating').each(function () {
            $.Velocity.mock = true;
            $(this).velocity("finish");
            $.Velocity.mock = false;
        });

        uvSlideHide(activeItem, dir);

        var d = 1;

        if (dir == 'prev') {
            d = -1;
        } else if (dir == 'stay') {
            d = 0;
        }

        var nextSlideIndex = activeItemIndex + d;
        if (toIndex != undefined) {
            nextSlideIndex = toIndex;
        } else if (nextSlideIndex < 0) {
            nextSlideIndex = uvItemsLength - 1;
        } else if (nextSlideIndex > uvItemsLength - 1) {
            nextSlideIndex = 0;
        }

        parent.find('.uvSlider__pagination__item').eq(nextSlideIndex).addClass('active').siblings().removeClass('active');

        var slide = parent.find('.uvSliderItem').eq(nextSlideIndex),
            slideImage = slide.find('.uvSliderItem__mainImage'),
            slideText = slide.find('.uvSliderItem__mainText');

        if (swipedir != undefined && swipedir == 'left' || swipedir == 'right') {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir);
        } else {
            uvSlideShowNext(activeItem, slide, slideImage, slideText, dir);
        }

    }
}

$('.uvSlider__nav').on('click', function () {
    var dir = $(this).data('direction'),
        parent = $(this).parents('.uvSlider'),
        swipedir = undefined,
        toIndex = undefined,
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent);
});

$('.uvSlider__pagination__item').on('click', function () {
    var dir = 'next',
        parent = $(this).parents('.uvSlider'),
        swipedir = undefined,
        toIndex = $(this).index(),
        activeItem = parent.find('.uvSliderItem.active'),
        activeItemIndex = activeItem.index(),
        uvItemsLength = parent.find('.uvSliderItem').length;

    if ($(this).index() < parent.find('.uvSlider__pagination__item.active').index()) {
        dir = 'prev';
    }
    if (!$(this).hasClass('active')) {
        initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent);
    }
});



var swiperElements = document.getElementsByClassName('uvSliderItemsList');
// var swipeEl = document.getElementById('uvSliderItemsList');

// if (swipeElements) {
for (let index = 0; index < swiperElements.length; index++) {
    const element = swiperElements[index];

    swipedetect(element, function (swipedir) {
        var dir = 'prev',
            parent = $(element).parents('.uvSlider'),
            swipedir = swipedir,
            toIndex = undefined,
            activeItem = parent.find('.uvSliderItem.active'),
            activeItemIndex = activeItem.index(),
            uvItemsLength = parent.find('.uvSliderItem').length;

        if (swipedir == 'left') {
            dir = 'next'
        }
        if (swipedir != 'none') {
            initUvSliderMove(dir, swipedir, toIndex, activeItem, activeItemIndex, uvItemsLength, parent);
        }
    });

}
// }


if ($('._height_recalc').length) {
    $('._height_recalc').parents('.uvSliderItemsList').outerHeight(718)
}
// -- mainPage slider

// ++ smooth appearance
function moveT(el, dir, appearDelay, appearSpace = 70) {
    if (appearDelay == undefined) {
        appearDelay = 1000;
    } else {
        appearDelay = parseInt(appearDelay) + 1000;
    }
    if (dir == 'toTop') {
        el.velocity({
            translateY: [0, appearSpace],
            opacity: [1, 0]
        }, {
            duration: appearDelay,
            easing: 'easeInSine'
        });
    } else if (dir == 'toLeft') {
        el.velocity({
            translateX: [0, appearSpace],
            opacity: [1, 0]
        }, {
            duration: appearDelay,
            easing: 'easeInOut'
        });
    } else if (dir == 'toRight') {
        el.velocity({
            translateX: [0, -appearSpace],
            opacity: [1, 0]
        }, {
            duration: appearDelay,
            easing: 'easeInOut'
        });
    } else {
        el.velocity({
            opacity: [1, 0]
        }, {
            duration: appearDelay,
            easing: 'easeInSine'
        });

    }
    el.addClass('alreadyAnimated');
}

$.fn.isInViewport = function () {
    var elementTop = $(this).offset().top;
    var elementBottom = elementTop + $(this).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + ($(window).height() / 1.1);
    return elementBottom > viewportTop && elementTop < viewportBottom;
};

var delayElem = document.querySelectorAll('.appearAnimated');

for (var i in delayElem) if (delayElem.hasOwnProperty(i)) {
    delayElem[i].style.opacity = 0;
}

$(window).on('scroll load', function () {
    $('.appearAnimated').each(function (el) {
        var appearDir = $(this).data('dir'),
            appearDelay = $(this).data('delay'),
            appearSpace = $(this).data('space');

        if ($(this).isInViewport() && !$(this).hasClass('alreadyAnimated')) {
            moveT($(this), appearDir, appearDelay, appearSpace);
        }
    });
});
// -- smooth appearance