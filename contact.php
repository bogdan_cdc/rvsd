<?php // Initialize variables to null.
    $name =""; // Sender Name
    $email =""; // Sender's email ID
    $purpose =""; // Subject of mail
    $message =""; // Sender's Message
    $phone ="";
    $nameError ="";
    $emailError ="";
    $purposeError ="";
    $messageError ="";
    $successMessage =""; // On submittingform below function will execute.
    $errors = [];
    $success = 0;

    if(isset($_POST['email'])) { // Checking null values in message.
        if (empty($_POST["name"])){
            $errors[] = "Поле обязательно для заполнения";
        } else {
            $name = test_input($_POST["name"]); // check name only contains letters and whitespace
            if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                $errors[] = "Можно использовать только буквы и пробелы";
            }
        } // Checking null values inthe message.
        
        if (empty($_POST["email"])) {
            $errors[] = "Поле обязательно для заполнения";
        } else {
            $email = test_input($_POST["email"]);
        } // Checking null values inmessage.

        if (empty($_POST["phone"])) {
            $errors[] = "Поле обязательно для заполнения";
        } else {
            $phone = test_input($_POST["phone"]);
        } // Checking null values inmessage.
        
        if (empty($_POST["purpose"])) {
            $errors[] = "Purpose is required";
        } else {
            $purpose = test_input($_POST["purpose"]);
        } // Checking null values inmessage.
        
        if (empty($_POST["message"])) {
            $errors[] = "Поле обязательно для заполнения";
        }
        else  {
            $message = test_input($_POST["message"]);
        } // Checking null values inthe message.
        if( !($name=='') && !($email=='') && !($purpose=='') && !($message=='') ) { // Checking valid email.
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $header= $name."<". $email .">";
                $headers = "fdg.uz"; /* Let's prepare the message for the e-mail */
                $msg = "Здравствуйте, $name! Спасибо за вашу заявку!
                Имя: $name
                E-mail: $email
                Телефон: $phone
                Сообщение: $message
                Вы получили это письмо потому что оставили заявку на сайте fdg.uz
                Мы свяжемся с вами в ближайшее время.";
                $msg1 = " Заявка на сайте fdg.uz
                Имя: $name
                E-mail: $email
                Телефон: $phone
                Тема: $purpose
                Сообщение: $message "; /* Send the message using mail() function */
                
                if(mail($email, $headers, $msg ) && mail("bogdan.cdc@gmail.com", $header, $msg1 )) {
                    $success = 1;
                }
            } else{
                $errors[] = 'Указан не валидный e-mail!';
            }
        } else {
            $errors[] = 'Одно из обязательных полей не заполнено!';
        }
    } // Function for filtering input values.function test_input($data)
    
    echo array('errors' => $errors, 'success' => $success);
    
    function test_input($data) {
        $data = trim($data);
        $data =stripslashes($data);
        $data =htmlspecialchars($data);
        return $data;
    }
?>