// var handlebars = require('gulp-compile-handlebars');
// var rename = require('gulp-rename');

var gulp = require('gulp'),
    sass = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sass'),
    sourcemaps = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-sourcemaps'),
    autoprefixer = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-autoprefixer'),
    browserSync = require('C:/Users/user/AppData/Roaming/npm/node_modules/browser-sync').create(),
    scsslint = require('C:/Users/user/AppData/Roaming/npm/node_modules/gulp-scss-lint'),
    gulpif = require('gulp-if');

gulp.task('scss-lint', function() {
  return gulp.src('./scss/*.scss')
    .pipe(scsslint());
});

var paths = {
  css: ['./scss/*.scss'],
  hbs: ['./pages/*.hbs']
};


var options = {};

options.autoprefixer = {
  browsers: [
  "Android 2.3",
  "Android >= 4",
  "Chrome >= 20",
  "Firefox >= 24",
  "Explorer >= 7",
  "iOS >= 6",
  "Opera >= 12",
  "Safari >= 6"
  ]
};

//html


gulp.task('templates', function () {
    var templateData = {
        firstName: 'app'
    },
    options1 = {
        batch : './pages/'
    }
    return gulp.src('./pages/')
        .pipe(handlebars(templateData, options1))
        .pipe(gulp.dest('./dist/'));
});


//  css

gulp.task('prepros', function(){
    setTimeout(function(event, cb){
      // console.log(416546546164);
      return gulp.src(paths.css)
      .pipe(sourcemaps.init())
      .pipe(sass({
        outputStyle: 'expanded'
      }).on('error', sass.logError))
      .pipe(autoprefixer(options.autoprefixer))
      .pipe(sourcemaps.write('./css/'))
      .pipe(gulp.dest('./css/'))
    }, 200);
});


gulp.task('watcher',function(){
    // gulp.watch(paths.hbs, ['templates']);
    gulp.watch(paths.css, ['prepros']);
});

browserSync.init({
  watch: true,
  server: "./",
  port: 8765
});

gulp.task('default', ['watcher']);